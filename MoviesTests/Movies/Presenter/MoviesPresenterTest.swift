@testable import Movies
import XCTest

class MoviesPresenterTest: XCTestCase {
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: MoviesListInteractorInput {
        func getMovies() {
        }
    }

    class MockViewController: MoviesViewInput {
        func setupInitialState() {
            
        }

        func reload() {

        }

        func showError(title: String, message: String) {
        }
    }
}
