import UIKit

class MoviesModuleConfigurator {
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? MoviesViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: MoviesViewController) {
        let presenter = MoviesListPresenter()
        presenter.view = viewController

        let interactor = MoviesListInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }
}
