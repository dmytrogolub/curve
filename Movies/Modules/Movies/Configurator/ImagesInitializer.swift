import UIKit

class MoviesModuleInitializer: NSObject {
    // Connect with object on storyboard
    @IBOutlet var moviesViewController: MoviesViewController!

    override func awakeFromNib() {
        let configurator = MoviesModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: moviesViewController)
    }
}
