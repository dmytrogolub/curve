import UIKit

class MoviesListPresenter: MoviesViewOutput, MoviesListInteractorOutput {
    weak var view: MoviesViewInput!
    var interactor: MoviesListInteractorInput!
    var selectedImageIndex: IndexPath?

    var moviesList: [Movie] = [] {
        didSet {
            view.reload()
        }
    }

    func viewIsReady() {
        interactor.getMovies()
    }

    func title() -> String {
        return NSLocalizedString("Popular Movies", comment: "")
    }

    func numberOfItemsInSection() -> Int {
        return moviesList.count
    }

    func movies(_ movies: [Movie]) {
        if moviesList.isEmpty {
            moviesList = movies
        } else {
            moviesList.append(contentsOf: movies)
        }
        view.reload()
    }

    func error(_ error: MoviesServiceError) {
        var errorMessage = "Movie DB service request error"
        switch error {
        case .apiError(let error as NSError):
            errorMessage = error.localizedDescription
        default:
            break
        }
        view.showError(title: "Error", message: errorMessage)
    }

    func dataModel(with index: IndexPath) -> Movie {
        if index.row == (moviesList.count - 1) {
            interactor.getMovies()
        }
        return moviesList[index.row]
    }

    func movieSelected(at index: IndexPath) {
        selectedImageIndex = index
    }

    func prepare(for segue: UIStoryboardSegue) {
        guard let dest = segue.destination as? FullScreenViewController,
            let module = dest.output as? FullScreenModuleInput,
            let index = selectedImageIndex else {
            return
        }
        module.imageURL = moviesList[index.row].imageURL
        module.title = moviesList[index.row].movieTitle
    }
}
