import Foundation
import UIKit

let greenColour = UIColor(red: 0, green: 0.6588, blue: 0.251, alpha: 1.0)

struct Movie {
    let imageURL: String
    let movieTitle: String
    let movieDescription: String
    let rating: NSAttributedString
    let releaseDate: String

    init?(url: String?, title: String?, overview: String?, vote: NSAttributedString?, date: String?) {
        if let url = url,
            let title = title,
            let overview = overview,
            let vote = vote,
            let date = date {
            imageURL = url
            movieTitle = title
            movieDescription = overview
            rating = vote
            releaseDate = date
        } else {
            return nil
        }
    }
}

class MoviesListInteractor: MoviesListInteractorInput {
    weak var output: MoviesListInteractorOutput!
    private let moviesService: MoviesService

    var numberOfPages = 0
    var currentPage = 0

    lazy var dateFormatterFrom: DateFormatter = {
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()

    lazy var dateFormatterTo: DateFormatter = {
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        return dateFormatter
    }()

    init(service: MoviesService = MoviesServiceImpl()) {
        moviesService = service
    }

    func formatDate(_ date: String?) -> String? {
        guard let date = date else {
            return nil
        }

        if let d = dateFormatterFrom.date(from: date) {
            return dateFormatterTo.string(from: d)
        }
        return date
    }

    func colour(for vote: Double) -> UIColor {
        if vote >= 7.0 {
            return greenColour
        } else if vote >= 4.0 {
            return UIColor.orange
        }
        return UIColor.red
    }

    func vote(for rating: Double) -> String {
        // I assume here that the max vote on TMDB is 10. I haven't found this information in API describtion
        return String(format: "%.0f%%", (rating / 10.0) * 100.0)
    }

    func formatVotes(rating: Double?) -> NSAttributedString? {
        guard let rating = rating else {
            return nil
        }
        let stringColor = [NSAttributedStringKey.foregroundColor: self.colour(for: rating)]
        return NSAttributedString(string: vote(for: rating), attributes: stringColor)
    }

    func movieImageURL(id: String?) -> String? {
        guard let id = id else {
            return nil
        }
        return "http://image.tmdb.org/t/p/w500/\(id)"
    }

    func getMovies() {
        moviesService.movies(with: currentPage + 1) { [weak self] result in
            switch result {
            
            case .success(let movies):
                self?.currentPage = movies.page!
                self?.numberOfPages = movies.totalPages!
                let movies = movies
                    .movies?
                    .map {
                        Movie(url: self?.movieImageURL(id: $0.poster_path),
                              title: $0.title,
                              overview: $0.overview,
                              vote: self?.formatVotes(rating: $0.voteAverage),
                              date: self?.formatDate($0.releaseDate))
                    }
                    .compactMap { $0 }
                DispatchQueue.main.async {
                    self?.output.movies(movies ?? [])
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.output.error(error)
                }
            }
        }
    }
}
