import Foundation

protocol MoviesListInteractorOutput: class {
    func movies(_: [Movie])
    func error(_: MoviesServiceError)
}
