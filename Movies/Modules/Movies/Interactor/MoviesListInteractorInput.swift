import Foundation

protocol MoviesListInteractorInput {
    func getMovies()
}
