import UIKit

protocol MoviesViewOutput {
    func viewIsReady()
    func numberOfItemsInSection() -> Int
    func dataModel(with index: IndexPath) -> Movie
    func movieSelected(at index: IndexPath)
    func prepare(for segue: UIStoryboardSegue)
    func title() -> String
}
