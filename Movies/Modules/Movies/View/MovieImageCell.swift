import Kingfisher
import UIKit

class MovieImageCell: UICollectionViewCell {
    @IBOutlet private var image: UIImageView!
    @IBOutlet private var movieTitle: UILabel!
    @IBOutlet private var moviesDescription: UILabel!
    @IBOutlet private var releaseDate: UILabel!
    @IBOutlet private var rating: UILabel!

    func configure(_ model: Movie) {
        image.kf.indicatorType = .activity
        image.kf.setImage(with: URL(string: model.imageURL)!)
        movieTitle.text = model.movieTitle
        moviesDescription.text = model.movieDescription
        releaseDate.text = model.releaseDate
        rating.attributedText = model.rating
    }
}
