import Foundation

protocol MoviesViewInput: class {
    func setupInitialState()
    func reload()
    func showError(title: String, message: String)
}
