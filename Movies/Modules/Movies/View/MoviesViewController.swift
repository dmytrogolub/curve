import UIKit

class MoviesViewController: UIViewController, MoviesViewInput {
    var output: MoviesViewOutput!

    @IBOutlet private var collectionView: UICollectionView!

    let columnLayout = MoviesListFlowLayout(
        cellsPerRow: 1,
        minimumInteritemSpacing: 5,
        minimumLineSpacing: 5,
        sectionInset: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    )

    // MARK: Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.collectionViewLayout = columnLayout
        collectionView.contentInsetAdjustmentBehavior = .always
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "",
                                                           style: .plain,
                                                           target: nil,
                                                           action: nil)

        navigationItem.title = output.title()
        output.viewIsReady()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func reload() {
        collectionView.reloadData()
    }

    // MARK: ImagesViewInput

    func setupInitialState() {
    }

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        output.prepare(for: segue)
    }
}

extension MoviesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_: UICollectionView, numberOfItemsInSection _: Int) -> Int {
        return output.numberOfItemsInSection()
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchImage", for: indexPath)
        if let cell = cell as? MovieImageCell {
            cell.configure(output.dataModel(with: indexPath))
        }

        return cell
    }

    func collectionView(_: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        output.movieSelected(at: indexPath)
        performSegue(withIdentifier: "FullScreenViewController", sender: self)
    }
}
