protocol FullScreenViewOutput {
    func viewIsReady()
    func imageStringURL() -> String
    var title: String { get }
}
