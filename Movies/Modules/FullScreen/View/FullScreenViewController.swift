import Kingfisher
import UIKit

class FullScreenViewController: UIViewController, FullScreenViewInput {
    var output: FullScreenViewOutput!

    @IBOutlet var image: UIImageView!

    // MARK: Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
        navigationItem.title = output.title
    }

    // MARK: FullScreenViewInput

    func setupInitialState() {
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        image.kf.setImage(with: URL(string: output.imageStringURL())!)
    }
}
