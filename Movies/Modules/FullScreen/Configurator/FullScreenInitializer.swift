import UIKit

class FullScreenModuleInitializer: NSObject {
    // Connect with object on storyboard
    @IBOutlet var fullscreenViewController: FullScreenViewController!

    override func awakeFromNib() {
        let configurator = FullScreenModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: fullscreenViewController)
    }
}
