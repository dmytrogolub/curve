class FullScreenPresenter: FullScreenModuleInput, FullScreenViewOutput {
    weak var view: FullScreenViewInput!
    var title: String = ""

    func viewIsReady() {
    }

    func imageStringURL() -> String {
        return imageURL
    }

    var imageURL: String = "" {
        didSet {
        }
    }
}
