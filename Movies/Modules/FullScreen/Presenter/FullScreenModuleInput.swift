protocol FullScreenModuleInput: class {
    var imageURL: String { get set }
    var title: String { get set }
}
